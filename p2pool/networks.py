from p2pool.bitcoin import networks
from p2pool.util import math

# CHAIN_LENGTH = number of shares back client keeps
# REAL_CHAIN_LENGTH = maximum number of shares back client uses to compute payout
# REAL_CHAIN_LENGTH must always be <= CHAIN_LENGTH
# REAL_CHAIN_LENGTH must be changed in sync with all other clients
# changes can be done by changing one, then the other

nets = dict(
    neoscoinsha256d=math.Object(
        PARENT=networks.nets['neoscoinsha256d'],
        SHARE_PERIOD=30, # seconds
        CHAIN_LENGTH=24*60*60//15, # shares
        REAL_CHAIN_LENGTH=24*60*60//15, # shares
        TARGET_LOOKBEHIND=200, # shares  //with that the pools share diff is adjusting faster, important if huge hashing power comes to the pool
        SPREAD=3, # blocks
        IDENTIFIER='47f1a4d388240564'.decode('hex'),
        PREFIX='589610df0a0493f5'.decode('hex'),
        P2P_PORT=3333,
        MIN_TARGET=0,
        MAX_TARGET=2**256//2**32 - 1,
        PERSIST=False,
        WORKER_PORT=3334,
        BOOTSTRAP_ADDRS='pool.altmine.net 99.236.215.227'.split(' '),
        ANNOUNCE_CHANNEL='#p2pool-neos',
        VERSION_CHECK=lambda v: True,
    ),
    neoscoinx11=math.Object(
        PARENT=networks.nets['neoscoinx11'],
        SHARE_PERIOD=30, # seconds
        CHAIN_LENGTH=24*60*60//15, # shares
        REAL_CHAIN_LENGTH=24*60*60//15, # shares
        TARGET_LOOKBEHIND=200, # shares  //with that the pools share diff is adjusting faster, important if huge hashing power comes to the pool
        SPREAD=3, # blocks
        IDENTIFIER='53a95b540399edcf'.decode('hex'),
        PREFIX='e72d7d72b941ed01'.decode('hex'),
        P2P_PORT=3335,
        MIN_TARGET=0,
        MAX_TARGET=2**256//2**22 - 1,
        PERSIST=False,
        WORKER_PORT=3336,
        BOOTSTRAP_ADDRS='pool.altmine.net'.split(' '),
        ANNOUNCE_CHANNEL='#p2pool-neos',
        VERSION_CHECK=lambda v: True,
    ),
    neoscoinblake=math.Object(
        PARENT=networks.nets['neoscoinblake'],
        SHARE_PERIOD=30, # seconds
        CHAIN_LENGTH=24*60*60//15, # shares
        REAL_CHAIN_LENGTH=24*60*60//15, # shares
        TARGET_LOOKBEHIND=200, # shares  //with that the pools share diff is adjusting faster, important if huge hashing power comes to the pool
        SPREAD=3, # blocks
        IDENTIFIER='8cf24f3fe75e871c'.decode('hex'),
        PREFIX='c4fd7a76abe65fb6'.decode('hex'),
        P2P_PORT=3337,
        MIN_TARGET=0,
        MAX_TARGET=2**256//2**32 - 1,
        PERSIST=False,
        WORKER_PORT=3338,
        BOOTSTRAP_ADDRS='pool.altmine.net'.split(' '),
        ANNOUNCE_CHANNEL='#p2pool-neos',
        VERSION_CHECK=lambda v: True,
    ),
)
for net_name, net in nets.iteritems():
    net.NAME = net_name
