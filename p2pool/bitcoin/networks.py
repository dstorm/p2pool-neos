import os
import platform

from twisted.internet import defer

from . import data
from p2pool.util import math, pack, jsonrpc

nets = dict(
    neoscoinsha256d=math.Object(
        P2P_PREFIX='09080706'.decode('hex'),
        P2P_PORT=15005,
        ADDRESS_VERSION=53,
        RPC_PORT=15004,
        RPC_CHECK=defer.inlineCallbacks(lambda bitcoind: defer.returnValue(
            'neoscoinaddress' in (yield bitcoind.rpc_help()) and
            not (yield bitcoind.rpc_getinfo())['testnet']
        )),
        SUBSIDY_FUNC=lambda height: (50*100000000) >> (height+1)//210000,
        BLOCKHASH_FUNC=data.hash256,
        POW_FUNC=data.hash256,
        BLOCK_PERIOD=300, # s
        SYMBOL='NEOS',
        CONF_FILE_FUNC=lambda: os.path.join(os.path.join(os.environ['APPDATA'], 'NeosCoin') if platform.system() == 'Windows' else os.path.expanduser('~/Library/Application Support/NeosCoin/') if platform.system() == 'Darwin' else os.path.expanduser('~/.neoscoin'), 'neoscoin.conf'),
        BLOCK_EXPLORER_URL_PREFIX='http://explorer.neoscoin.com/block/',
        ADDRESS_EXPLORER_URL_PREFIX='http://explorer.neoscoin.com/address/',
        TX_EXPLORER_URL_PREFIX='http://explorer.neoscoin.com/tx/',
        SANE_TARGET_RANGE=(2**256//2**32//1000 - 1, 2**256//2**32 - 1),
        DUMB_SCRYPT_DIFF=1,
        DUST_THRESHOLD=0.001e8,
    ),
    neoscoinx11=math.Object(
        P2P_PREFIX='09080706'.decode('hex'),
        P2P_PORT=15005,
        ADDRESS_VERSION=53,
        RPC_PORT=15004,
        RPC_CHECK=defer.inlineCallbacks(lambda bitcoind: defer.returnValue(
            'neoscoinaddress' in (yield bitcoind.rpc_help()) and
            not (yield bitcoind.rpc_getinfo())['testnet']
        )),
        SUBSIDY_FUNC=lambda height: (50*100000000) >> (height+1)//210000,
        BLOCKHASH_FUNC=data.hash256,
        POW_FUNC=lambda data: pack.IntType(256).unpack(__import__('xcoin_hash').getPoWHash(data)),
        BLOCK_PERIOD=300, # s
        SYMBOL='NEOS',
        CONF_FILE_FUNC=lambda: os.path.join(os.path.join(os.environ['APPDATA'], 'NeosCoin') if platform.system() == 'Windows' else os.path.expanduser('~/Library/Application Support/NeosCoin/') if platform.system() == 'Darwin' else os.path.expanduser('~/.neoscoin'), 'neoscoin.conf'),
        BLOCK_EXPLORER_URL_PREFIX='http://explorer.neoscoin.com/block/',
        ADDRESS_EXPLORER_URL_PREFIX='http://explorer.neoscoin.com/address/',
        TX_EXPLORER_URL_PREFIX='http://explorer.neoscoin.com/tx/',
        SANE_TARGET_RANGE=(2**256//2**32//1000 - 1, 2**256//2**22 - 1),
        DUMB_SCRYPT_DIFF=1,
        DUST_THRESHOLD=0.001e8,
    ),
    neoscoinblake=math.Object(
        P2P_PREFIX='09080706'.decode('hex'),
        P2P_PORT=15005,
        ADDRESS_VERSION=53,
        RPC_PORT=15004,
        RPC_CHECK=defer.inlineCallbacks(lambda bitcoind: defer.returnValue(
            'neoscoinaddress' in (yield bitcoind.rpc_help()) and
            not (yield bitcoind.rpc_getinfo())['testnet']
        )),
        SUBSIDY_FUNC=lambda height: (50*100000000) >> (height+1)//210000,
        BLOCKHASH_FUNC=data.hash256,
        POW_FUNC=lambda data: pack.IntType(256).unpack(__import__('blake_sfr_hash').getHash(data, 80)),
        BLOCK_PERIOD=300, # s
        SYMBOL='NEOS',
        CONF_FILE_FUNC=lambda: os.path.join(os.path.join(os.environ['APPDATA'], 'NeosCoin') if platform.system() == 'Windows' else os.path.expanduser('~/Library/Application Support/NeosCoin/') if platform.system() == 'Darwin' else os.path.expanduser('~/.neoscoin'), 'neoscoin.conf'),
        BLOCK_EXPLORER_URL_PREFIX='http://explorer.neoscoin.com/block/',
        ADDRESS_EXPLORER_URL_PREFIX='http://explorer.neoscoin.com/address/',
        TX_EXPLORER_URL_PREFIX='http://explorer.neoscoin.com/tx/',
        SANE_TARGET_RANGE=(2**256//2**32//1000 - 1, 2**256//2**32 - 1),
        DUMB_SCRYPT_DIFF=1,
        DUST_THRESHOLD=0.001e8,
    ),
)
for net_name, net in nets.iteritems():
    net.NAME = net_name
